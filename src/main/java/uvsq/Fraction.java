/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uvsq;

/**
 *
 * @author User
 */
public class Fraction {
    private int num;
    private int den;
    
    public Fraction (int num,int den){
    	this.num=num ;
    	this.den=den ;
    }
    public Fraction (int num) {
    	this.num=num;
    	den=1;
    }
    public Fraction () {
    	this.num=0;
    	this.den=1;
    }
     //getteurs //
    public int getNum() {
    	return num;
    }
    public int getDen() {
    	return den;
    }
    

    public String toString () {
    	 return "le numerateur :"+num+ "et le denominateur "+den;
    }
    
    public double consultation () {
    	return (double)num / (double)den ;
    }
      public String conversion (){
    	return Integer.toString(num)+" "+Integer.toString(den);
    }
      public int compNat (Fraction f1,Fraction f2){
    	if (f1.num*f2.den<f1.den*f2.num) return 1 ;
    	else if (f1.num*f2.den>f1.den*f2.num)  return -1;
    	else return 0;
      }
      public boolean TestEgalite ( Fraction f1,Fraction f2) {
    	if (f1.consultation()==f2.consultation()) return true;
    	else return false;
    }
}
